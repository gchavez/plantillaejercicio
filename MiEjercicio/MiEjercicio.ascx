﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MiEjercicio.ascx.vb" Inherits="DotNetNuke.Modules.Cursos.MiEjercicio" %>
<script type="text/javascript">
dificultad = <%=dificultad %>;
</script>
<link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("MiEjercicio.css") %>" />
<script src="<%=ResolveUrl("MiEjercicio.js") %>" type="text/javascript"></script>

<div id="Actividad_Main">
    <div class="pantalla pantall1" id="pantallaInstruccion">
		<div class="tituloGeneralEspecial">
			Instrucciones</div>
		<div class="subtituloStyleGeneral" id="instruccionesTitulo">
			<asp:Label ID="lblInstruccionesTitulo" runat="server"></asp:Label>
		</div>
		<div class="textoInstruccionesEjercicioCaja" runat="server" id="instruccionesTexto">               
			<asp:Label ID="lblInstruccionesTexto" runat="server"></asp:Label>
		</div>
		<div id="cont-btnEmpezar">
			<input type="button" id="btnEmpezar" class="btnInstruccionesContinuar manita" style="background-image: url('<%=UrlEjercicio %>/images/<%=me.skin %>/btn_iniciar_ejercicio.png');" /></div>
	</div>
    <div class="pantalla pantalla2 unselectable" id="pantallaActividad">
	    video killed radio start
		
		<script>
			alert("Funcionando");
		</script>
		
    </div>
    <div class="pantalla unselectable pantall3" id="pantallaPausa">

    </div>
    <div id="pantallaResultado" class="pantalla pantalla4">
        <div class="tituloGeneralEspecial">Resultado</div>
        <div id="resultadoEstadistica" style="font-size: 18pt;margin-top: 10%;text-align: center;">
		Obtuviste el <span id="lblRespuestasPorcentaje"></span>% correcto
		</div>
        <div id="contResultado">
                
                <div style="font-size:12px; padding-left: 30px;">
                    <span><span id="lblRespuestasCorrectas" style="color:green;">0</span> Respuestas Correctas</span><br>
                    <span><span id="lblRespuestasIncorrectas" style="color:red;">0</span> Respuestas Incorrectas</span><br>
                </div>


            <div class="contBoton" id="contFinal" style="text-align: center;width: 80%; margin-left: auto; margin-right: auto;margin-top: 10%;">
                <asp:Button runat="server" ID="btnEnviar" class="manita btnFinalizaEjercicio buttton buttton-green"  />
                <span id="spanProcesar" class="btnProcesando buttton buttton-gray">Procesando</span>
            </div>
        </div>
    </div>

</div>
