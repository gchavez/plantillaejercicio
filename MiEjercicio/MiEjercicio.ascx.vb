﻿Imports DotNetNuke
Imports DotNetNuke.Modules
Imports DotNetNuke.Entities.Modules
Imports DotNetNuke.Entities.Modules.PortalModuleBase
Imports System.Web
Namespace DotNetNuke.Modules.Cursos


    Partial Class MiEjercicio



        Inherits DotNetNuke.Entities.Modules.PortalModuleBase
        Friend skin As String = ""
        Public Event Finalizar()
        Public Event ErrorGenerado(ByVal mensaje As String)
        Friend plantilla As String = ""
        Friend texto As String
        Friend palabrasEjercicios As String = ""
        Friend porcentajeMinimoCorrecto as Integer = 60
        Friend dificultad as Integer = 0



        Public Sub Inicializar(Optional ByVal skinConsola As String = "skin1")
            Try                
                'id dev = 11540
                'identificar edad alumno
				Dim dlEjercicio As New dlEjercicio
                Me.lblInstruccionesTitulo.Text = dlEjercicio.ObtenerEjercicio(dlEjercicio.ObtenerIDEjercicio).Nombre
                Me.lblInstruccionesTexto.Text = dlEjercicio.ObtenerEjercicio(dlEjercicio.ObtenerIDEjercicio).Instrucciones
                skin = skinConsola
                plantilla = dlEjercicio.ObtenerEjercicio(dlEjercicio.ObtenerIDEjercicio).PlantillaConsola
                
                Select Case plantilla
                    Case "MiEjercicio1"
                        dificultad = 0
                    Case "MiEjercicio2"
                        dificultad = 1
                    Case "MiEjercicio3"
                        dificultad = 2
                End Select

            Catch ex As Exception
                RaiseEvent ErrorGenerado(ex.ToString)
            End Try
        End Sub
        Protected Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
            RaiseEvent Finalizar()
        End Sub	
    End Class
End Namespace


